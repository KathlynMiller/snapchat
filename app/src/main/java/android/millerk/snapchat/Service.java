package android.millerk.snapchat;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.List;


public class Service extends IntentService {


    public Service() {
        super("Service");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
          String action = intent.getAction();
            if(action.equals(Constants.ACTION_ADD_FRIEND)){
                String firstUserName = intent.getStringExtra("firstUserName");
                String secondUserName = intent.getStringExtra("secondUserName");
                Log.i("Service","Service adding friend. First user: " + firstUserName + ". Second user: " + secondUserName);

            }
        }
    }

    private void addFriends(String firstUserName, String secondUserName) {
        BackenlessDataQuery guery = new BackendlessDataQuery();
        query.setWhereClause(String.format("name = '%s' or name = '%s", firstUserName, secondUserName));
        Backendless.Persistance.of(BackendlessUser.class).find(query, new AsyncCallback<BackendlessCollection> {


        @Override
        public void handleResponse (BackendlessCollection <BackendlessUser> response) {
                List<BackendlessUser> users = response.getData();
                if (users.size() != 2) {
                    broadcastAddFriendFailure();
                } else {
                    BackendlessUser user1 = users.get(0);
                    BackendlessUser user2 = users.get(1);

                    updateFriendList(user1, user2);
                    Backendless.UserService.update(user1, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser user) {

                            updateFriendsList(user2, user);
                            Backendless.UserService.update(user2, new AsyncCallback<BackendlessUser>
                            {
                                @Override
                                public void handleRespone (BackendlessUser response){
                                broadcastAddFriendSuccess();
                            }

                                @Override
                                public void handleFault (BackendlessFault fault){
                                broadcastAddFriendFailure();

                            }

                            });
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            broadcastAddFriendFailure();
                        }
                    });
                }
            }
            @Override
            public void handleFault(BackendlessFault fault) {
                broadcastAddFriendFailure();
            }

            private void broadcastAddFriendSuccess(){
               Intent intent = new Intent(Constants.BROADCAST_ADD_FRIEND_SUCCESS);
                sendBroadcast(intent);
            }

            private void broadcastAddfriendFailure(){
                Intent intent = new Intent(Constants.BROADCAST_ADD_FRIEND_FAILURE);
                sendBroadcast(intent);
            }



           }

       });
    }

    private void updateFriendsList(Backendless user, BackendlessUser friend){
        BackendlessUser[] new Friends;

        Object[] currentFriendOBjects = (Object[]) user.getProperty("friend");
        if(currentFriendObjects.length > 0){
            Backendless[] currentFriends = (BackendlessUser[]) currentFriendObjects;
            newFriends = new BackendlessUser[currentFriends.length + 1];
            for(int i = 0; i < currentFriends.length; i++){
                newFriends[i] = currentFriends[i];
            }
            newFriends[newFriends.length -1] = friend;
        }else{
            newFriends = new BackendlessUser[]{ friend };
        }
        user.setProperty("friends", newFriends);
    }



}
