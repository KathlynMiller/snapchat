package android.millerk.snapchat;


public class Constants {

    public static final String ACTION_ADD_FRIEND = "android.millerk.snapchat.ADD_FRIEND";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "android.millerk.snapchat.SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "android.millerk.snapchat.FAILURE";

}
