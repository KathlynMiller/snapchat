package android.millerk.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ProfileFragment profile = new ProfileFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.profileContainer, profile).commit();
    }
}
