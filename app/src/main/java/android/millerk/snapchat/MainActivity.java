package android.millerk.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID ="8D2F863B-3D1D-0570-FF2C-8C25F271B700";
    public static final String SECRET_KEY = "180FA379-0E4D-0D98-FF5D-F30F305B8F00";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser() == "") {
            LoginMenu login = new LoginMenu();
            getSupportFragmentManager().beginTransaction().add(R.id.container, login).commit();
        }else {
            MainMenuFragment camera = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, camera).commit();
        }
    }
}
