package android.millerk.snapchat;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {


    public MainMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);

        String[] menuItems = {"Profile",
        "Friends", "Messages","Settings","Camera"};

        ListView listView = (ListView)view.findViewById(R.id.mainMenu);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItems
        );
        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(getActivity(), Profile.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "Profile", Toast.LENGTH_SHORT).show();
                } else if (position == 1) {
                    Intent intent2 = new Intent(getActivity(), Friends.class);
                    startActivity(intent2);
                    Toast.makeText(getActivity(), "Friends", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Intent intent3 = new Intent(getActivity(), Messages.class);
                    startActivity(intent3);
                    Toast.makeText(getActivity(), "Messages", Toast.LENGTH_SHORT).show();
                } else if (position == 3) {
                    Intent intent4 = new Intent(getActivity(), Settings.class);
                    startActivity(intent4);
                    Toast.makeText(getActivity(), "Settings", Toast.LENGTH_SHORT).show();
                } else if (position == 4) {
                    Intent intent5 = new Intent(getActivity(), LoggedInFragment.class);
                    startActivity(intent5);
                    Toast.makeText(getActivity(), "Log out", Toast.LENGTH_SHORT).show();
                } else if (position == 5) {
                    Intent intent6 = new Intent(getActivity(), CameraFragment.class);
                    startActivity(intent6);
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                } else if (position == 6) {
                    Intent intent7 = new Intent(getActivity(), AddFriendFragment.class);
                    startActivity(intent7);
                    Toast.makeText(getActivity(), "Add Friend", Toast.LENGTH_SHORT).show();


                }


            }):
        // Inflate the layout for this fragment

        }

          return view;


    }

}
